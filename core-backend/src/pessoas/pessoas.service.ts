import { Injectable, Logger } from '@nestjs/common';
import { Pessoa } from './interface/pessoas.entity';
import { RpcException } from '@nestjs/microservices';
import { InjectEntityManager, InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';

@Injectable()
export class PessoasService {
  constructor(
    @InjectRepository(Pessoa)
    private readonly pessoaRepository: Repository<Pessoa>,
  ) {}

  private readonly logger = new Logger(PessoasService.name);

  async criarPessoa(pessoa: Pessoa): Promise<void> {
    try {
      await this.pessoaRepository.save(pessoa);
    } catch (error) {
      this.logger.error(`error: ${JSON.stringify(error.message)}`);
      throw new RpcException(error.message);
    }
  }

  async consultarPessoa(data: string[]): Promise<Pessoa[]> {
    if (data['co_cpf']) {
      try {
        return this.pessoaRepository.find({
          /* select: [
            'ID_PESSOA',
            'NO_PESSOA',
            'CO_CPF',
            'DT_NASCIMENTO',
            'DT_ATUALIZACAO_RECEITA',
          ], */
          where: [{ CO_CPF: data['co_cpf'] }],
        });
      } catch (error) {
        this.logger.error(`error: ${JSON.stringify(error.message)}`);
        throw new RpcException(error.message);
      }
    }

    if (data['id_pessoa']) {
      try {
        return this.pessoaRepository.find({
          /* select: [
            'ID_PESSOA',
            'NO_PESSOA',
            'CO_CPF',
            'DT_NASCIMENTO',
            'DT_ATUALIZACAO_RECEITA',
          ], */
          where: [{ ID_PESSOA: data['id_pessoa'] }],
        });
      } catch (error) {
        this.logger.error(`error: ${JSON.stringify(error.message)}`);
        throw new RpcException(error.message);
      }
    }

    if (data['co_matricula_tjdft']) {
      try {
        return this.pessoaRepository.find({
          /* select: [
            'ID_PESSOA',
            'NO_PESSOA',
            'CO_CPF',
            'DT_NASCIMENTO',
            'DT_ATUALIZACAO_RECEITA',
          ], */
          where: [{ CO_MATRICULA_TJDFT: data['co_matricula_tjdft'] }],
        });
      } catch (error) {
        this.logger.error(`error: ${JSON.stringify(error.message)}`);
        throw new RpcException(error.message);
      }
    }

    if (data['no_pessoa']) {
      try {
        return this.pessoaRepository.find({
          /* select: [
            'ID_PESSOA',
            'NO_PESSOA',
            'CO_CPF',
            'DT_NASCIMENTO',
            'DT_ATUALIZACAO_RECEITA',
          ], */
          where: [{ NO_PESSOA: data['no_pessoa'] }],
        });
      } catch (error) {
        this.logger.error(`error: ${JSON.stringify(error.message)}`);
        throw new RpcException(error.message);
      }
    }
  }
}
