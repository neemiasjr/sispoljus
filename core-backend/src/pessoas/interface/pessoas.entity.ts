import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tb_pessoa_nestjs')
export class Pessoa {
  @PrimaryGeneratedColumn({ type: 'int' })
  ID_PESSOA: number;

  @Index('NO_PESSOA_INDEX')
  @Column({ type: 'varchar', length: 150, nullable: true })
  NO_PESSOA: string;

  @Column({ type: 'varchar', length: 1, nullable: true })
  CO_SEXO: string;

  @Column({ type: 'date', nullable: true })
  DT_NASCIMENTO: Date;

  @Column({ type: 'varchar', length: 150, nullable: true })
  NO_MAE: string;

  @Column({ type: 'varchar', length: 150, nullable: true })
  NO_PAI: string;

  @Index('CO_CPF_INDEX')
  @Column({ type: 'varchar', length: 11, nullable: true })
  CO_CPF: string;

  @Column({ type: 'int', nullable: true })
  ID_NATURALIDADE: number;

  @Column({ type: 'int', nullable: true })
  ID_NACIONALIDADE: number;

  @Index('CO_MATRICULA_TJDFT_INDEX')
  @Column({ type: 'varchar', length: 20, nullable: true })
  CO_MATRICULA_TJDFT: string;

  @Column({ type: 'int', nullable: true })
  id_uf: number;

  @Index('CO_RG_INDEX')
  @Column({ type: 'varchar', length: 20, nullable: true })
  CO_RG: string;

  @Column({ type: 'int', nullable: true })
  id_uf_rg: number;

  @Column({ type: 'int', nullable: true })
  co_carteira_trabalho: number;

  @Column({ type: 'int', nullable: true })
  co_seria_carteira_trabalho: number;

  @Column({ type: 'int', nullable: true })
  id_uf_carteira_trabalho: number;

  @Column({ type: 'varchar', length: 20, nullable: true })
  co_passaporte: string;

  @Column({ type: 'int', nullable: true })
  id_pais_emissor_passaporte: number;

  @Column({ type: 'varchar', length: 7, nullable: true })
  co_oab: string;

  @Column({ type: 'int', nullable: true })
  id_uf_oab: number;

  @Column({ type: 'int', nullable: true })
  ic_trocar_foto: number;

  @Column({ type: 'int', nullable: true })
  ic_conferir_documento: number;

  @Column({ type: 'varchar', length: 20, nullable: true })
  co_id_estrangeiro: string;

  @Column({ type: 'int', nullable: true })
  co_pais_id_estrangeiro: number;

  @Column({ type: 'int', nullable: true })
  Instituicao_idInstituicao: number;

  @Column({ type: 'char', length: 200, nullable: true })
  tx_observacao: string;

  @Column({ type: 'date', nullable: true })
  DT_ATUALIZACAO_RECEITA: Date;
}
