import { Controller, Logger } from '@nestjs/common';
import { PessoasService } from './pessoas.service';
import {
  EventPattern,
  Payload,
  Ctx,
  RmqContext,
  MessagePattern,
} from '@nestjs/microservices';
import { Pessoa } from './interface/pessoas.entity';

const ackErrors: string[] = ['E11000'];

@Controller()
export class PessoasController {
  logger = new Logger(PessoasController.name);
  constructor(private readonly pessoasService: PessoasService) {}

  @EventPattern('criar-pessoa')
  async criarJogador(@Payload() pessoa: Pessoa, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    try {
      this.logger.log(`pessoa: ${JSON.stringify(pessoa)}`);
      await this.pessoasService.criarPessoa(pessoa);
      await channel.ack(originalMsg);
    } catch (error) {
      this.logger.log(`error: ${JSON.stringify(error.message)}`);
      const filterAckError = ackErrors.filter((ackError) =>
        error.message.includes(ackError),
      );

      if (filterAckError.length > 0) {
        await channel.ack(originalMsg);
      }
    }
  }

  @MessagePattern('consultar-pessoas')
  async consultarPessoas(
    @Payload() data: string[],
    @Ctx() context: RmqContext,
  ) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    try {
      if (data) {
        this.logger.log(`Dados microservices: ${JSON.stringify(data)}`);
        return await this.pessoasService.consultarPessoa(data);
      }
    } finally {
      await channel.ack(originalMsg);
    }
  }
}
