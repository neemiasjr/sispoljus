import { Module } from '@nestjs/common';
import { PessoasModule } from './pessoas/pessoas.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    PessoasModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'sidenwebapp',
      password: '123456',
      database: 'sidenweb',
      autoLoadEntities: true,
      synchronize: true,
      //debug: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
