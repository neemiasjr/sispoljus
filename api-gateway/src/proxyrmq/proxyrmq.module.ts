import { Module } from '@nestjs/common';
import { ClientProxySisPoljus } from './client-proxy';

@Module({
  providers: [ClientProxySisPoljus],
  exports: [ClientProxySisPoljus],
})
export class ProxyRMQModule {}
