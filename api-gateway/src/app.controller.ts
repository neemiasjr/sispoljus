import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import {
  AllowAnyRole,
  Public,
  Roles,
  Unprotected,
} from 'nest-keycloak-connect';

@Controller('api/v1')
export class AppController {
  private readonly logger = new Logger(AppService.name);
  constructor(private readonly appService: AppService) {}

  @Get('/public')
  @Unprotected()
  getpublic(): string {
    this.logger.log(``);
    return `${this.appService.getHello()} from public`;
  }
  @Get('/user')
  @Roles('agente')
  getUser(): string {
    return `${this.appService.getHello()} from user`;
  }
  @Get('/admin')
  @Roles()
  getAdmin(): string {
    return `${this.appService.getHello()} from admin`;
  }
  @Get('/all')
  @AllowAnyRole()
  getAll(): string {
    return `${this.appService.getHello()} from all`;
  }
}
