import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import * as momentTimezone from 'moment-timezone';
import { AllExceptionsFilter } from './common/filters/http-exception.filter';
import { LoginInterceptor } from './common/interceptors/loggin.interceptors';
import { TimeoutInterceptor } from './common/interceptors/timeout.interceptors';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const logger = new Logger('App');
  logger.log('Starting app...');

  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('Entidade Pessoa')
    .setDescription('Sidenweb')
    .setVersion('1.0')
    .addTag('sispoljus')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  app.useGlobalInterceptors(new LoginInterceptor(), new TimeoutInterceptor());

  app.useGlobalFilters(new AllExceptionsFilter());

  const port = parseInt(process.env.NODE_PORT, 10) || 3000;

  Date.prototype.toJSON = function (): any {
    return momentTimezone(this)
      .tz('America/Sao_Paulo')
      .format('YYYY-MM-DD HH:mm:ss.SSS');
  };

  await app.listen(port);

  await logger.log(
    `App started on port ${port}... - SSO_URL: ${process.env.KEYCLOAK_AUTH_URI}`,
  );
}
bootstrap();
