import { Controller, Get, Logger, Query, UseFilters } from '@nestjs/common';
import { ValidacaoParametrosPipe } from '../common/pipes/validacao-parametros.pipe';
import { InjectRepository } from '@nestjs/typeorm';
import { Public, Roles, Unprotected } from 'nest-keycloak-connect';
import { Observable } from 'rxjs';
import { ClientProxySisPoljus } from '../proxyrmq/client-proxy';

@Controller('api/v1/pessoas')
export class PessoasController {
  private logger = new Logger(PessoasController.name);

  constructor(private clientProxySisPoljus: ClientProxySisPoljus) {}

  private clientCoreBackend =
    this.clientProxySisPoljus.getClientProxyCoreBackendInstante();

  @Get()
  consultarPessoa(@Query() data: string[]): Observable<any> {
    //return this.pessoasService.findOne(cpf);
    this.logger.log(`Dados consultados: ${JSON.stringify(data)}`);

    return this.clientCoreBackend.send('consultar-pessoas', data ? data : '');
  }
}
