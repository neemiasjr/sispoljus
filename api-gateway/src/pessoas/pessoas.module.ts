import { Module } from '@nestjs/common';
import { PessoasController } from './pessoas.controller';
import { ProxyRMQModule } from '../proxyrmq/proxyrmq.module';

@Module({
  imports: [ProxyRMQModule],
  controllers: [PessoasController],
})
export class PessoasModule {}
